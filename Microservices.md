* Auth service: https://gitlab.com/Josteiol/schoolteams-service-auth
* Mail service: https://gitlab.com/andreasmi/schoolteams-service-recovery
* Api Gateway: https://gitlab.com/andreasmi/schoolteams-zuul-gateway
* Administrator service: https://gitlab.com/andreasmi/schoolteams-service-administrator
* Coach service: https://gitlab.com/lhskage/schoolteams-service-coach
* Parent service: https://gitlab.com/lhskage/schoolteams-service-parent
* Player service: https://gitlab.com/lhskage/schoolteams-service-player
* GDPR service: https://gitlab.com/lhskage/schoolteams-service-gdpr
* School Teams frontend: https://gitlab.com/tbhatt/school-teams-frontend