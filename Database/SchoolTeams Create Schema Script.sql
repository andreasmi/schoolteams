--
-- PostgreSQL database dump
--

-- Dumped from database version 12.4
-- Dumped by pg_dump version 13.0

-- Started on 2020-10-28 12:30:00

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE defaultdb;
--
-- TOC entry 4073 (class 1262 OID 16395)
-- Name: defaultdb; Type: DATABASE; Schema: -; Owner: avnadmin
--

CREATE DATABASE defaultdb WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.UTF-8';


ALTER DATABASE defaultdb OWNER TO avnadmin;

\connect defaultdb

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 202 (class 1259 OID 16411)
-- Name: child_parent; Type: TABLE; Schema: public; Owner: avnadmin
--

CREATE TABLE public.child_parent (
    parent_1 text NOT NULL,
    child text NOT NULL,
    parent_2 text,
    CONSTRAINT parent1_parent2_different CHECK ((parent_1 <> parent_2))
);


ALTER TABLE public.child_parent OWNER TO avnadmin;

--
-- TOC entry 203 (class 1259 OID 16418)
-- Name: location_id_seq; Type: SEQUENCE; Schema: public; Owner: avnadmin
--

CREATE SEQUENCE public.location_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.location_id_seq OWNER TO avnadmin;

--
-- TOC entry 204 (class 1259 OID 16420)
-- Name: location; Type: TABLE; Schema: public; Owner: avnadmin
--

CREATE TABLE public.location (
    id integer DEFAULT nextval('public.location_id_seq'::regclass) NOT NULL,
    street text,
    number text,
    name text,
    postal_code text
);


ALTER TABLE public.location OWNER TO avnadmin;

--
-- TOC entry 205 (class 1259 OID 16427)
-- Name: match_id_seq; Type: SEQUENCE; Schema: public; Owner: avnadmin
--

CREATE SEQUENCE public.match_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.match_id_seq OWNER TO avnadmin;

--
-- TOC entry 206 (class 1259 OID 16429)
-- Name: match; Type: TABLE; Schema: public; Owner: avnadmin
--

CREATE TABLE public.match (
    id integer DEFAULT nextval('public.match_id_seq'::regclass) NOT NULL,
    start_time timestamp without time zone NOT NULL,
    cancelled boolean DEFAULT false NOT NULL,
    comment text DEFAULT ''::text,
    location_id integer NOT NULL,
    team_1_id integer NOT NULL,
    team_2_id integer NOT NULL,
    CONSTRAINT home_away_different CHECK ((team_1_id <> team_2_id))
);


ALTER TABLE public.match OWNER TO avnadmin;

--
-- TOC entry 207 (class 1259 OID 16439)
-- Name: match_result; Type: TABLE; Schema: public; Owner: avnadmin
--

CREATE TABLE public.match_result (
    home_team_goals integer,
    away_team_goals integer,
    match_id integer NOT NULL,
    winner text
);


ALTER TABLE public.match_result OWNER TO avnadmin;

--
-- TOC entry 208 (class 1259 OID 16445)
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: avnadmin
--

CREATE SEQUENCE public.message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.message_id_seq OWNER TO avnadmin;

--
-- TOC entry 209 (class 1259 OID 16447)
-- Name: message; Type: TABLE; Schema: public; Owner: avnadmin
--

CREATE TABLE public.message (
    sender text NOT NULL,
    receiver text NOT NULL,
    message text NOT NULL,
    message_id integer DEFAULT nextval('public.message_id_seq'::regclass) NOT NULL
);


ALTER TABLE public.message OWNER TO avnadmin;

--
-- TOC entry 210 (class 1259 OID 16454)
-- Name: optional_id_seq; Type: SEQUENCE; Schema: public; Owner: avnadmin
--

CREATE SEQUENCE public.optional_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.optional_id_seq OWNER TO avnadmin;

--
-- TOC entry 211 (class 1259 OID 16456)
-- Name: optional; Type: TABLE; Schema: public; Owner: avnadmin
--

CREATE TABLE public.optional (
    id integer DEFAULT nextval('public.optional_id_seq'::regclass) NOT NULL,
    date_of_birth timestamp without time zone,
    mobile_number text,
    profile_picture text,
    medical_notes text,
    dob_shared boolean DEFAULT false,
    mobile_number_shared boolean DEFAULT false,
    profile_picture_shared boolean DEFAULT false,
    medical_notes_shared boolean DEFAULT false
);


ALTER TABLE public.optional OWNER TO avnadmin;

--
-- TOC entry 212 (class 1259 OID 16467)
-- Name: person; Type: TABLE; Schema: public; Owner: avnadmin
--

CREATE TABLE public.person (
    id integer NOT NULL,
    firstname text,
    lastname text,
    email text,
    gender text,
    optional_id integer
);


ALTER TABLE public.person OWNER TO avnadmin;

--
-- TOC entry 213 (class 1259 OID 16473)
-- Name: person_id_seq; Type: SEQUENCE; Schema: public; Owner: avnadmin
--

CREATE SEQUENCE public.person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_id_seq OWNER TO avnadmin;

--
-- TOC entry 4074 (class 0 OID 0)
-- Dependencies: 213
-- Name: person_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: avnadmin
--

ALTER SEQUENCE public.person_id_seq OWNED BY public.person.id;


--
-- TOC entry 214 (class 1259 OID 16475)
-- Name: player; Type: TABLE; Schema: public; Owner: avnadmin
--

CREATE TABLE public.player (
    user_username text NOT NULL,
    team_id integer NOT NULL,
    player_number text NOT NULL
);


ALTER TABLE public.player OWNER TO avnadmin;

--
-- TOC entry 215 (class 1259 OID 16481)
-- Name: postal_code; Type: TABLE; Schema: public; Owner: avnadmin
--

CREATE TABLE public.postal_code (
    post_code text NOT NULL,
    post_city text NOT NULL
);


ALTER TABLE public.postal_code OWNER TO avnadmin;

--
-- TOC entry 216 (class 1259 OID 16487)
-- Name: role; Type: TABLE; Schema: public; Owner: avnadmin
--

CREATE TABLE public.role (
    id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.role OWNER TO avnadmin;

--
-- TOC entry 217 (class 1259 OID 16493)
-- Name: school_id_seq; Type: SEQUENCE; Schema: public; Owner: avnadmin
--

CREATE SEQUENCE public.school_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.school_id_seq OWNER TO avnadmin;

--
-- TOC entry 218 (class 1259 OID 16495)
-- Name: school; Type: TABLE; Schema: public; Owner: avnadmin
--

CREATE TABLE public.school (
    id integer DEFAULT nextval('public.school_id_seq'::regclass) NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.school OWNER TO avnadmin;

--
-- TOC entry 219 (class 1259 OID 16502)
-- Name: school_change_suggestion; Type: TABLE; Schema: public; Owner: avnadmin
--

CREATE TABLE public.school_change_suggestion (
    school_id integer NOT NULL,
    name text NOT NULL,
    comment text NOT NULL,
    approved boolean DEFAULT false NOT NULL,
    requester text NOT NULL
);


ALTER TABLE public.school_change_suggestion OWNER TO avnadmin;

--
-- TOC entry 220 (class 1259 OID 16509)
-- Name: sport_id_seq; Type: SEQUENCE; Schema: public; Owner: avnadmin
--

CREATE SEQUENCE public.sport_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sport_id_seq OWNER TO avnadmin;

--
-- TOC entry 221 (class 1259 OID 16511)
-- Name: sport; Type: TABLE; Schema: public; Owner: avnadmin
--

CREATE TABLE public.sport (
    id integer DEFAULT nextval('public.sport_id_seq'::regclass) NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.sport OWNER TO avnadmin;

--
-- TOC entry 222 (class 1259 OID 16518)
-- Name: team_id_seq; Type: SEQUENCE; Schema: public; Owner: avnadmin
--

CREATE SEQUENCE public.team_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.team_id_seq OWNER TO avnadmin;

--
-- TOC entry 223 (class 1259 OID 16520)
-- Name: team; Type: TABLE; Schema: public; Owner: avnadmin
--

CREATE TABLE public.team (
    id integer DEFAULT nextval('public.team_id_seq'::regclass) NOT NULL,
    name text NOT NULL,
    school_id integer NOT NULL,
    coach text NOT NULL,
    sport_id integer NOT NULL
);


ALTER TABLE public.team OWNER TO avnadmin;

--
-- TOC entry 224 (class 1259 OID 16527)
-- Name: test_id_seq; Type: SEQUENCE; Schema: public; Owner: avnadmin
--

CREATE SEQUENCE public.test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.test_id_seq OWNER TO avnadmin;

--
-- TOC entry 225 (class 1259 OID 16529)
-- Name: users; Type: TABLE; Schema: public; Owner: avnadmin
--

CREATE TABLE public.users (
    username text NOT NULL,
    password text NOT NULL,
    person_id integer NOT NULL,
    role_id integer NOT NULL,
    tfa_qr_url text,
    tfa_key text,
    "2fa_active" boolean DEFAULT false,
    invited boolean DEFAULT false
);


ALTER TABLE public.users OWNER TO avnadmin;

--
-- TOC entry 3875 (class 2604 OID 16537)
-- Name: person id; Type: DEFAULT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.person ALTER COLUMN id SET DEFAULT nextval('public.person_id_seq'::regclass);


--
-- TOC entry 3883 (class 2606 OID 16539)
-- Name: child_parent child_parent_child_key; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.child_parent
    ADD CONSTRAINT child_parent_child_key UNIQUE (child);


--
-- TOC entry 3885 (class 2606 OID 16541)
-- Name: child_parent child_parent_pkey; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.child_parent
    ADD CONSTRAINT child_parent_pkey PRIMARY KEY (child);


--
-- TOC entry 3887 (class 2606 OID 16543)
-- Name: location location_pkey; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.location
    ADD CONSTRAINT location_pkey PRIMARY KEY (id);


--
-- TOC entry 3889 (class 2606 OID 16545)
-- Name: match match_pkey; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.match
    ADD CONSTRAINT match_pkey PRIMARY KEY (id);


--
-- TOC entry 3891 (class 2606 OID 16547)
-- Name: match_result match_result_pkey; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.match_result
    ADD CONSTRAINT match_result_pkey PRIMARY KEY (match_id);


--
-- TOC entry 3893 (class 2606 OID 16549)
-- Name: message message_pkey; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT message_pkey PRIMARY KEY (message_id);


--
-- TOC entry 3895 (class 2606 OID 16551)
-- Name: optional optional_pkey; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.optional
    ADD CONSTRAINT optional_pkey PRIMARY KEY (id);


--
-- TOC entry 3897 (class 2606 OID 16553)
-- Name: person person_email_key; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT person_email_key UNIQUE (email);


--
-- TOC entry 3899 (class 2606 OID 16555)
-- Name: person person_pkey; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (id);


--
-- TOC entry 3901 (class 2606 OID 16557)
-- Name: player player_pkey; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.player
    ADD CONSTRAINT player_pkey PRIMARY KEY (user_username, team_id);


--
-- TOC entry 3903 (class 2606 OID 16559)
-- Name: player player_team_id_player_number_key; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.player
    ADD CONSTRAINT player_team_id_player_number_key UNIQUE (team_id, player_number);


--
-- TOC entry 3905 (class 2606 OID 16561)
-- Name: postal_code postal_code_pkey; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.postal_code
    ADD CONSTRAINT postal_code_pkey PRIMARY KEY (post_code);


--
-- TOC entry 3907 (class 2606 OID 16563)
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- TOC entry 3911 (class 2606 OID 16565)
-- Name: school_change_suggestion school_change_suggestion_pkey; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.school_change_suggestion
    ADD CONSTRAINT school_change_suggestion_pkey PRIMARY KEY (school_id, name);


--
-- TOC entry 3909 (class 2606 OID 16567)
-- Name: school school_pkey; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.school
    ADD CONSTRAINT school_pkey PRIMARY KEY (id);


--
-- TOC entry 3913 (class 2606 OID 16569)
-- Name: sport sport_pkey; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.sport
    ADD CONSTRAINT sport_pkey PRIMARY KEY (id);


--
-- TOC entry 3915 (class 2606 OID 16571)
-- Name: team team_name_school_id_key; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.team
    ADD CONSTRAINT team_name_school_id_key UNIQUE (name, school_id);


--
-- TOC entry 3917 (class 2606 OID 16573)
-- Name: team team_pkey; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.team
    ADD CONSTRAINT team_pkey PRIMARY KEY (id);


--
-- TOC entry 3919 (class 2606 OID 16575)
-- Name: users users_person_id_key; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_person_id_key UNIQUE (person_id);


--
-- TOC entry 3921 (class 2606 OID 16577)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (username);


--
-- TOC entry 3922 (class 2606 OID 16578)
-- Name: child_parent fk_child_parent_user1; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.child_parent
    ADD CONSTRAINT fk_child_parent_user1 FOREIGN KEY (parent_1) REFERENCES public.users(username);


--
-- TOC entry 3923 (class 2606 OID 16583)
-- Name: child_parent fk_child_parent_user2; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.child_parent
    ADD CONSTRAINT fk_child_parent_user2 FOREIGN KEY (parent_2) REFERENCES public.users(username);


--
-- TOC entry 3924 (class 2606 OID 16588)
-- Name: child_parent fk_child_parent_user3; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.child_parent
    ADD CONSTRAINT fk_child_parent_user3 FOREIGN KEY (child) REFERENCES public.users(username);


--
-- TOC entry 3926 (class 2606 OID 16593)
-- Name: match fk_location_match; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.match
    ADD CONSTRAINT fk_location_match FOREIGN KEY (location_id) REFERENCES public.location(id);


--
-- TOC entry 3929 (class 2606 OID 16598)
-- Name: match_result fk_match_result; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.match_result
    ADD CONSTRAINT fk_match_result FOREIGN KEY (match_id) REFERENCES public.match(id);


--
-- TOC entry 3932 (class 2606 OID 16603)
-- Name: person fk_person_optional1; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT fk_person_optional1 FOREIGN KEY (optional_id) REFERENCES public.optional(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3933 (class 2606 OID 16608)
-- Name: player fk_player_team; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.player
    ADD CONSTRAINT fk_player_team FOREIGN KEY (team_id) REFERENCES public.team(id);


--
-- TOC entry 3934 (class 2606 OID 16613)
-- Name: player fk_player_users; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.player
    ADD CONSTRAINT fk_player_users FOREIGN KEY (user_username) REFERENCES public.users(username);


--
-- TOC entry 3925 (class 2606 OID 16680)
-- Name: location fk_postal_code_location; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.location
    ADD CONSTRAINT fk_postal_code_location FOREIGN KEY (postal_code) REFERENCES public.postal_code(post_code);


--
-- TOC entry 3935 (class 2606 OID 16618)
-- Name: school_change_suggestion fk_school_change; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.school_change_suggestion
    ADD CONSTRAINT fk_school_change FOREIGN KEY (school_id) REFERENCES public.school(id);


--
-- TOC entry 3937 (class 2606 OID 16623)
-- Name: team fk_school_team; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.team
    ADD CONSTRAINT fk_school_team FOREIGN KEY (school_id) REFERENCES public.school(id);


--
-- TOC entry 3938 (class 2606 OID 16628)
-- Name: team fk_sport_team; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.team
    ADD CONSTRAINT fk_sport_team FOREIGN KEY (sport_id) REFERENCES public.sport(id);


--
-- TOC entry 3927 (class 2606 OID 16633)
-- Name: match fk_team_match_away; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.match
    ADD CONSTRAINT fk_team_match_away FOREIGN KEY (team_2_id) REFERENCES public.team(id);


--
-- TOC entry 3928 (class 2606 OID 16638)
-- Name: match fk_team_match_home; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.match
    ADD CONSTRAINT fk_team_match_home FOREIGN KEY (team_1_id) REFERENCES public.team(id);


--
-- TOC entry 3936 (class 2606 OID 16643)
-- Name: school_change_suggestion fk_user_change_requester; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.school_change_suggestion
    ADD CONSTRAINT fk_user_change_requester FOREIGN KEY (requester) REFERENCES public.users(username);


--
-- TOC entry 3940 (class 2606 OID 16648)
-- Name: users fk_user_person; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk_user_person FOREIGN KEY (person_id) REFERENCES public.person(id) ON UPDATE CASCADE;


--
-- TOC entry 3941 (class 2606 OID 16653)
-- Name: users fk_user_role; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk_user_role FOREIGN KEY (role_id) REFERENCES public.role(id);


--
-- TOC entry 3930 (class 2606 OID 16658)
-- Name: message fk_users_message1; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT fk_users_message1 FOREIGN KEY (sender) REFERENCES public.users(username);


--
-- TOC entry 3931 (class 2606 OID 16663)
-- Name: message fk_users_message2; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT fk_users_message2 FOREIGN KEY (receiver) REFERENCES public.users(username);


--
-- TOC entry 3939 (class 2606 OID 16668)
-- Name: team fk_users_team_coach; Type: FK CONSTRAINT; Schema: public; Owner: avnadmin
--

ALTER TABLE ONLY public.team
    ADD CONSTRAINT fk_users_team_coach FOREIGN KEY (coach) REFERENCES public.users(username);


-- Completed on 2020-10-28 12:30:06

--
-- PostgreSQL database dump complete
--

